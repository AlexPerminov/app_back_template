package com.back.dto;

import com.back.entity.Role;
import com.back.entity.User;
import lombok.Data;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Data
public class UserView {
    String pic;
    String name;
    UUID id;
    String token;
    String username;
    List<String> roles;

    public UserView(User user) {
        this.username = user.getUsername();
        this.name = user.getName();
        this.pic = user.getPic();
        this.id = user.getId();
        this.roles = user.getRoles().stream().map(Role::getName).collect(Collectors.toList());
    }
}
