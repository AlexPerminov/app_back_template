package com.back.services;

import com.back.entity.User;
import com.back.exceptions.UserAlreadyExistException;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public interface UserService {

    User register(User user) throws UserAlreadyExistException;

    List<User> getAll();

    User findByUsername(String username);

    User findById(UUID id);

    void delete(UUID id);

    String processOAuthPostLogin(Map<String, Object> attr);
}
