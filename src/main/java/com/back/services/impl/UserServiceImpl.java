package com.back.services.impl;

import com.back.entity.Role;
import com.back.entity.Status;
import com.back.entity.User;
import com.back.exceptions.UserAlreadyExistException;
import com.back.repo.UserRepository;
import com.back.security.jwt.JwtTokenProvider;
import com.back.services.RoleService;
import com.back.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

import static java.lang.String.format;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RoleService roleService;
    private final BCryptPasswordEncoder passwordEncoder;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;

    @Value("${app.admin.role-name}")
    private  String ADMIN_ROLE_NAME;
    @Value("${app.user.role-name}")
    private  String USER_ROLE_NAME;

    public UserServiceImpl(
            @Lazy UserRepository userRepository,
            RoleService roleService,
            @Lazy BCryptPasswordEncoder passwordEncoder,
            @Lazy AuthenticationManager authenticationManager,
            @Lazy JwtTokenProvider jwtTokenProvider
    ) {
        this.userRepository = userRepository;
        this.roleService = roleService;
        this.passwordEncoder = passwordEncoder;
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @Override
    public String processOAuthPostLogin(Map<String, Object> attr) {
        User client = userRepository.findByUsername((String) attr.get("email"));

        if (client == null) {
            Role roleUser = roleService.findByName(USER_ROLE_NAME);
            List<Role> userRoles = new ArrayList<>();
            userRoles.add(roleUser);
            User newClient = new User();
            newClient.setRoles(userRoles);
            newClient.setStatus(Status.ACTIVE);
            newClient.setCreatedAt(new Date());
            newClient.setUsername((String) attr.get("email"));
            newClient.setEmail((String) attr.get("email"));
            newClient.setName((String) attr.get("name"));
            newClient.setPic((String) attr.get("picture"));
            newClient.setPassword(passwordEncoder.encode((String) attr.get("sub"))); //убрать

            client = userRepository.save(newClient);

            log.info("IN register - client form google: {} successfully registered", client);
        }

        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(client.getUsername(), (String) attr.get("sub")));
        String token = jwtTokenProvider.createToken(client.getUsername(), client.getRoles());
        return token;
    }

    @Override
    public User register(User user) throws UserAlreadyExistException {
        checkUserExisting(user);
        Role roleAdmin = roleService.findByName(ADMIN_ROLE_NAME);
        Role roleUser = roleService.findByName(USER_ROLE_NAME);
        List<Role> userRoles = new ArrayList<>();
        userRoles.add(roleUser);
        userRoles.add(roleAdmin);

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setRoles(userRoles);
        user.setStatus(Status.ACTIVE);
        user.setCreatedAt(new Date());

        User registeredUser = userRepository.save(user);

        log.info("IN register - user: {} successfully registered", registeredUser);

        return registeredUser;
    }

    @Override
    public List<User> getAll() {
        List<User> result = userRepository.findAll();
        log.info("IN getAll - {} users found", result.size());
        return result;
    }

    @Override
    public User findByUsername(String username) {
        User result = userRepository.findByUsername(username);
        log.info("IN findByUsername - user: {} found by username: {}", result, username);
        return result;
    }

    @Override
    public User findById(UUID id) {
        User result = userRepository.findById(id).orElse(null);

        if (result == null) {
            log.warn("IN findById - no user found by id: {}", id);
            return null;
        }

        log.info("IN findById - user: {} found by id: {}", result);
        return result;
    }

    @Override
    public void delete(UUID id) {
        userRepository.deleteById(id);
        log.info("IN delete - user with id: {} successfully deleted");
    }

    private void checkUserExisting(User user) throws UserAlreadyExistException {
        User existingUser = userRepository.findByUsername(user.getUsername());
        if (existingUser != null) {
            log.info("IN register - user: {} already exists", existingUser);
            throw new UserAlreadyExistException(format("IN register - user: %s already exist", user.getUsername()));
        }
    }
}
