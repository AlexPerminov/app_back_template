package com.back.services.impl;

import com.back.entity.Role;
import com.back.repo.RoleRepository;
import com.back.services.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {
    private final RoleRepository roleRepository;

    @Autowired
    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public Role findByName(String name) {
        return roleRepository.findByName("ROLE_" + name);
    }

    @Override
    public Role save(Role role) {
        return roleRepository.save(role);
    }
}
