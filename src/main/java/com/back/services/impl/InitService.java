package com.back.services.impl;

import com.back.entity.Role;
import com.back.repo.RoleRepository;
import com.back.services.RoleService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class InitService {
    private final RoleService roleService;
    @Value("${app.admin.role-name}")
    private  String ADMIN_ROLE_NAME;
    @Value("${app.user.role-name}")
    private  String USER_ROLE_NAME;

    public InitService(RoleService roleService) {
        this.roleService = roleService;
    }

    @PostConstruct
    public void initRoles() {
        Role roleUser = roleService.findByName(USER_ROLE_NAME);
        if (roleUser == null) {
            Role userRole = new Role();
            userRole.setName(USER_ROLE_NAME);
            Role adminRole = new Role();
            adminRole.setName(ADMIN_ROLE_NAME);

            roleService.save(userRole);
            roleService.save(adminRole);
        }
    }
}
