package com.back.services;

import com.back.entity.Role;

public interface RoleService {
    Role findByName(String name);
    Role save(Role role);
}
