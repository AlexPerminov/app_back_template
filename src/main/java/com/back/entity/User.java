package com.back.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

import static java.lang.String.format;

@Entity()
@Table(name = "usr")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User extends BaseEntity {

    private String username;
    private String name;
    private String pic;
    private String password;
    private String email;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_roles",
            joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")})
    private List<Role> roles;

    @Override
    public String toString() {
        StringBuilder rolesAsString = new StringBuilder();
        if (roles != null) {
            roles.forEach(role -> rolesAsString.append(role.getName()));
        }
        return format("{%s, %s, [%s]}", super.getId(), username,rolesAsString );
    }
}
