package com.back.entity;

public enum Status {
    ACTIVE, NOT_ACTIVE, DELETED
}
