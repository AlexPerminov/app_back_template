package com.back.config;

import com.back.security.jwt.JwtConfigurer;
import com.back.security.jwt.JwtTokenProvider;
import com.back.security.oauth.CustomOAuth2UserService;
import com.back.security.oauth.CustomOauth2SuccessHandler;
import com.back.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;

import java.util.List;

///oauth2/authorization/google
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    private final JwtTokenProvider jwtTokenProvider;
    private final UserService userService;
    private final CustomOAuth2UserService oauthUserService;

    @Value("${app.front-end.url}")
    private String FRONT_END_URL;
    @Value("${app.admin.endpoint}")
    private String ADMIN_ENDPOINT;
    @Value("${app.login.endpoint}")
    private String LOGIN_ENDPOINT;
    @Value("${app.oauth.endpoint}")
    private String OAUTH_ENDPOINT;
    @Value("${app.admin.role-name}")
    private String ADMIN_ROLE_NAME;

    @Autowired
    public WebSecurityConfig(JwtTokenProvider jwtTokenProvider, UserService userService, CustomOAuth2UserService oauthUserService) {
        this.jwtTokenProvider = jwtTokenProvider;
        this.userService = userService;
        this.oauthUserService = oauthUserService;
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .httpBasic().disable()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .cors().configurationSource(corsCustomConfig())
                .and()
                .authorizeRequests()
                .antMatchers(LOGIN_ENDPOINT).permitAll()
                .antMatchers(OAUTH_ENDPOINT).permitAll()
                .antMatchers(ADMIN_ENDPOINT).hasRole(ADMIN_ROLE_NAME)
                .anyRequest().authenticated()
                .and()
                .apply(new JwtConfigurer(jwtTokenProvider))
                .and()
                .oauth2Login()
                .userInfoEndpoint()
                .userService(oauthUserService)
                .and()
                .successHandler(successHandler());
    }

    @Bean
    public CustomOauth2SuccessHandler successHandler() {
        return new CustomOauth2SuccessHandler(userService);
    }

    public CorsConfigurationSource corsCustomConfig() {
        CorsConfiguration cors = new CorsConfiguration();
        cors.setAllowedOrigins(List.of(FRONT_END_URL.split(",")));
        cors.setAllowedMethods(List.of("GET", "POST", "PUT", "DELETE", "OPTIONS"));
        cors.setAllowedHeaders(List.of("*"));
        return request -> cors;
    }
}
