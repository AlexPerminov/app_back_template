package com.back.exceptions;

public class AlreadyExistException extends Exception{
    public AlreadyExistException(String message) {
        super(message);
    }
}
