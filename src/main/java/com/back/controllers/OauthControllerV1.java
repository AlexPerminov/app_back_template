package com.back.controllers;

import com.back.repo.UserRepository;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

@RestController
public class OauthControllerV1 {

    private final UserRepository userRepository;

    @Autowired
    public OauthControllerV1(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("oauth2/{token}")
    public ResponseEntity<Object> getUserFromToken(@PathVariable String token) {
        try {
            Map<Object, Object> response = new HashMap<>();
            response.put("username", extractSubFromToken(token));
            response.put("token", token);
            return ResponseEntity.ok(response);
        } catch (Exception exception) {
            return ResponseEntity.badRequest().body(exception.getMessage());
        }
    }

    private String extractSubFromToken(String token) {
        String[] chunks = token.split("\\.");
        Base64.Decoder decoder = Base64.getUrlDecoder();

        String payload = new String(decoder.decode(chunks[1]));
        JSONObject jsonObject = new JSONObject(payload);
        return (String) jsonObject.get("sub");
    }
}
