package com.back.controllers;


import com.back.dto.RegistrationDto;
import com.back.dto.UserView;
import com.back.entity.User;
import com.back.exceptions.UserAlreadyExistException;
import com.back.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.security.Principal;

import static java.lang.String.format;

@RestController
@RequestMapping(value = "/api/v1/user")
public class UserControllerV1 {
    private final UserService userService;

    @Autowired
    public UserControllerV1(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("register")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Object> registration(@RequestBody RegistrationDto registrationDto) {
        User user = new User();

        user.setUsername(registrationDto.getUsername());
        user.setEmail(registrationDto.getUsername());
        user.setPassword(registrationDto.getPassword());
        try {
            userService.register(user);

            return ResponseEntity.ok(format("User %s created", user.getUsername()));
        } catch (UserAlreadyExistException exception) {
            return ResponseEntity.badRequest().body(exception.getMessage());
        }
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @GetMapping
    public ResponseEntity<Object> get(Principal principal) {
        try {
            User user = userService.findByUsername(principal.getName());
            UserView userView = new UserView(user);
            return ResponseEntity.ok(userView);
        } catch (EntityNotFoundException exception) {
            return ResponseEntity.badRequest().body(exception.getMessage());
        }
    }
}
