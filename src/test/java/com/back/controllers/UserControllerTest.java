package com.back.controllers;

import com.back.dto.RegistrationDto;
import com.back.entity.Role;
import com.back.entity.User;
import com.back.repo.UserRepository;
import com.back.security.jwt.JwtTokenProvider;
import com.back.services.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.net.PasswordAuthentication;
import java.security.Principal;
import java.util.List;
import java.util.UUID;

import static com.back.utils.TestUtils.createNewUser;
import static com.back.utils.TestUtils.getJsonString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SuppressWarnings("ALL")
@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class UserControllerTest {
    private static final String API_URL = "/api/v1/user";
    private static final String REGISTER_WORD = "/register";
    @Mock
    private UserService userService;
    @InjectMocks
    private UserControllerV1 userControllerV1;
    private MockMvc mockMvc;

    @BeforeEach
    void setup() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(userControllerV1).build();
    }

    @Test
    void getUser() throws Exception {
        String userName = "test";
        Principal mockPrincipal = Mockito.mock(Principal.class);
        Mockito.when(mockPrincipal.getName()).thenReturn(userName);
        Mockito.when(userService.findByUsername(userName))
                .thenReturn(createNewUser());
        mockMvc.perform(get(API_URL).principal(mockPrincipal))
                .andExpect(status().isOk());
    }

    @Test
    void registerUser() throws Exception {
        RegistrationDto registrationDto = new RegistrationDto();
        registrationDto.setUsername("test");
        registrationDto.setPassword("testPass");
        mockMvc.perform(
                post(API_URL + REGISTER_WORD)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(getJsonString(registrationDto))
        ).andExpect(status().isOk());
    }
}
