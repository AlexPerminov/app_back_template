package com.back.controllers;


import com.back.dto.AuthenticationRequestDto;
import com.back.entity.User;
import com.back.security.jwt.JwtTokenProvider;
import com.back.services.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.server.resource.BearerTokenAuthenticationToken;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static com.back.utils.TestUtils.createNewUser;
import static com.back.utils.TestUtils.getJsonString;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SuppressWarnings("ALL")
@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class AuthControllerTest {
    private static final String API_URL = "/api/v1/auth";
    private static final String LOGIN_WORD = "/login";
    @Mock
    private UserService userService;
    @Mock
    private AuthenticationManager authenticationManager;
    @Mock
    private JwtTokenProvider jwtTokenProvider;
    @InjectMocks
    private AuthControllerV1 authControllerV1;
    private MockMvc mockMvc;

    @BeforeEach
    void setup() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(authControllerV1).build();
    }

    @Test
    void login() throws Exception {
        AuthenticationRequestDto authenticationRequestDto = new AuthenticationRequestDto();
        User user = createNewUser();
        authenticationRequestDto.setUsername(user.getUsername());
        authenticationRequestDto.setPassword(user.getPassword());
        Mockito.when(userService.findByUsername(user.getUsername()))
                .thenReturn(user);
        Mockito.when(authenticationManager.authenticate(any(Authentication.class)))
                .thenReturn(new BearerTokenAuthenticationToken("test"));
        Mockito.when(jwtTokenProvider.createToken(user.getUsername(), user.getRoles())).thenReturn("test");
        mockMvc.perform(
                post(API_URL + LOGIN_WORD)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(getJsonString(authenticationRequestDto))
        ).andExpect(status().isOk());
    }
}
