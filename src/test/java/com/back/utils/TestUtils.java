package com.back.utils;

import com.back.entity.Role;
import com.back.entity.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.util.List;

public class TestUtils {
    public static User createNewUser() {
        Role role = new Role();
        role.setName("ROLE_TEST");

        return new User("test",
                "test",
                "test",
                "test",
                "test@test.test",
                List.of(role));
    }

    public static String getJsonString(Object object) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = objectMapper.writer().withDefaultPrettyPrinter();
        return ow.writeValueAsString(object);
    }
}
